package mappe.del2.PatientRegister.Utils;

import mappe.del2.PatientRegister.Models.Patient;
import mappe.del2.PatientRegister.Models.PatientRegister;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CSVFileTest {

    @Test
    @DisplayName("Test that .csv file with correct data is read")
    void readCSVFileWithCorrectData() {
        File file = new File("src/test/resources/CSVFileTest-correct-data.csv");

        assertNotNull(CSVFile.read(file));
    }

    @Test
    @DisplayName("Test reading an empty .csv file")
    void readEmptyCSVFile() {
        File file = new File("src/test/resources/CSVFileTest-empty.csv");

        assertTrue(CSVFile.read(file).isEmpty());
    }

    @Test
    @DisplayName("Test that .csv file gets written")
    void writeCSVFileWithPatients() {
        PatientRegister patientRegister = CSVFileTestData.fillPasientRegisterWithTestData(new PatientRegister());

        List<String[]> tmpPatients = new ArrayList<>();

        for (Patient patient : patientRegister.getPatients()) {
            String[] patientData = { patient.getFirstName(), patient.getLastName(), patient.getGeneralPractitioner(),
                    patient.getSocialSecurityNumber() };
            tmpPatients.add(patientData);
        }

        File file = new File("src/test/resources/CSVFileTest-write-patients.csv");

        CSVFile.write(tmpPatients, file);

        assertNotEquals(0, file.length());

    }
}
