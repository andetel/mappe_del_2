package mappe.del2.PatientRegister.Utils;

import mappe.del2.PatientRegister.Models.Patient;
import mappe.del2.PatientRegister.Models.PatientRegister;

public class CSVFileTestData {

    public static PatientRegister fillPasientRegisterWithTestData(PatientRegister register) {
        Patient patient1 = new Patient("12345678901", "Preben", "Etternavn", "Feber", "Doktor Proktor");
        Patient patient2 = new Patient("23456789012", "Jakob", "Ja", "Feber", "Doktor Proktor");
        Patient patient3 = new Patient("34567890123", "Gustav", "Noe", "Feber", "Doktor Proktor");
        Patient patient4 = new Patient("45678901234", "Lan", "Et", "Feber", "Doktor Proktor");
        Patient patient5 = new Patient("56789012345", "Rikke", "På");
        Patient patient6 = new Patient("67890123456", "Thea", "Nei");

        register.getPatients().add(patient1);
        register.getPatients().add(patient2);
        register.getPatients().add(patient3);
        register.getPatients().add(patient4);
        register.getPatients().add(patient5);
        register.getPatients().add(patient6);

        return register;
    }
}
