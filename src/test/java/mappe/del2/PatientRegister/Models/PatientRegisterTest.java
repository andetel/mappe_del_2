package mappe.del2.PatientRegister.Models;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PatientRegisterTest {

    @Test
    @DisplayName("Every patient is in the patient register list when retrieved")
    void getEveryPatient() {
        PatientRegister patientRegister = PatientRegisterTestData
                .fillPasientRegisterWithTestData(new PatientRegister());

        assertEquals(6, patientRegister.getPatients().size());
    }

    @Test
    @DisplayName("Correct patient is retrieved when searching by social security number")
    void getPatientBySSN() {
        PatientRegister patientRegister = PatientRegisterTestData
                .fillPasientRegisterWithTestData(new PatientRegister());

        Patient patient = patientRegister.getPatientBySSN("12345678901");

        assertEquals(patientRegister.getPatients().get(0), patient);
    }

    @Test
    @DisplayName("Patient is added to the list")
    void addPatient() throws IllegalAccessException {
        PatientRegister patientRegister = PatientRegisterTestData
                .fillPasientRegisterWithTestData(new PatientRegister());

        Patient patient = new Patient("78901234567", "Noe", "Annet", "Feber", "Proktor");
        patientRegister.addPatient(patient);

        assertTrue(patientRegister.getPatients().contains(patient));
    }

    @Test
    @DisplayName("Add duplicate patients to list")
    void addDuplicatePatient() throws IllegalAccessException {
        PatientRegister patientRegister = PatientRegisterTestData
                .fillPasientRegisterWithTestData(new PatientRegister());

        Patient patient = new Patient("78901234567", "Noe", "Annet", "Feber", "Proktor");
        Patient patient1 = new Patient("78901234567", "Noe", "Annet");

        patientRegister.addPatient(patient);

        assertThrows(IllegalArgumentException.class, () -> {
            patientRegister.addPatient(patient1);
        });
    }

    @Test
    @DisplayName("Patients first name is changed when trying to edit")
    void editPersonFirstName() {
        PatientRegister patientRegister = PatientRegisterTestData
                .fillPasientRegisterWithTestData(new PatientRegister());

        String name = patientRegister.getPatients().get(0).getFirstName();
        patientRegister.getPatients().get(0).setFirstName("Noe");

        assertNotEquals(name, patientRegister.getPatients().get(0).getFirstName());
    }

    @Test
    @DisplayName("Check that two patiants with same social security number are equal")
    void equalPatients() {
        PatientRegister patientRegister = PatientRegisterTestData
                .fillPasientRegisterWithTestData(new PatientRegister());

        Patient patient = new Patient("12345678901", "Per", "Ole");

        assertEquals(patient, patientRegister.getPatients().get(0));
    }

    @Test
    @DisplayName("Check that two patients with different social security number, but equal name is not equal")
    void notEqualPatients() {
        PatientRegister patientRegister = PatientRegisterTestData
                .fillPasientRegisterWithTestData(new PatientRegister());

        Patient patient = new Patient("90123456789", "Preben", "Etternavn");

        assertNotEquals(patient, patientRegister.getPatients().get(0));
    }
}
