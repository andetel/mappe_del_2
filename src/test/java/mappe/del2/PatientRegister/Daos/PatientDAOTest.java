package mappe.del2.PatientRegister.Daos;

import static org.junit.jupiter.api.Assertions.*;

import mappe.del2.PatientRegister.Models.Patient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.persistence.Persistence;
import javax.transaction.Transactional;
import java.sql.SQLException;

public class PatientDAOTest {
    private PatientDAO patientDAO = new PatientDAO(
            Persistence.createEntityManagerFactory("pu-register-derby-test").createEntityManager());
    private Patient patient;

    @BeforeEach
    void resetPatient() {
        patient = new Patient("12345678901", "Per", "Ladin", "Feber", "Proktor");
    }

    @Test
    @DisplayName("Patient gets created successfully")
    @Transactional
    void testPatientGetsCreatedSuccessfully() throws SQLException {
        patientDAO.createPatient(patient);

        assertTrue(patientDAO.checkExists(patient));
    }

    @Test
    @DisplayName("Test that patient data is correct after creation")
    void testPatientDataIsCorrectAfterCreation() throws SQLException {
        patientDAO.createPatient(patient);

        Patient patient1 = patientDAO.getPatient(patient.getSocialSecurityNumber()).get();

        boolean socialSecurityNumberOK = patient1.getSocialSecurityNumber().equals(patient.getSocialSecurityNumber());
        boolean firstNameOK = patient1.getSocialSecurityNumber().equals(patient.getSocialSecurityNumber());
        boolean lastNameOK = patient1.getLastName().equals(patient.getLastName());
        boolean diagnosisOK = patient1.getDiagnosis().equals(patient.getDiagnosis());
        boolean generalPractitionerOK = patient1.getGeneralPractitioner().equals(patient.getGeneralPractitioner());

        assertTrue(socialSecurityNumberOK && firstNameOK && lastNameOK && diagnosisOK && generalPractitionerOK);
    }

    @Test
    @DisplayName("Test that patient data is correct after update")
    void testPatientDataIsCorrectAfterUpdate() throws SQLException {
        patientDAO.createPatient(patient);

        String socialSecurityNumber = patientDAO.getPatient(patient.getSocialSecurityNumber()).get()
                .getSocialSecurityNumber();
        String firstName = "Noe";
        String lastName = "Noeannet";
        String diagnosis = "Broken leg";
        String generalPractitioner = "Doktor";

        Patient patient1 = new Patient(socialSecurityNumber, firstName, lastName, diagnosis, generalPractitioner);

        patientDAO.updatePatient(patient1);

        Patient patient2 = patientDAO.getPatient(patient.getSocialSecurityNumber()).get();

        boolean socialSecurityNumberOK = patient2.getSocialSecurityNumber().equals(socialSecurityNumber);
        boolean firstNameOK = patient2.getFirstName().equals(firstName);
        boolean lastNameOK = patient2.getLastName().equals(lastName);
        boolean diagnosisOK = patient2.getDiagnosis().equals(diagnosis);
        boolean generalPractitionerOK = patient2.getGeneralPractitioner().equals(generalPractitioner);

        assertTrue(socialSecurityNumberOK && firstNameOK && lastNameOK && diagnosisOK && generalPractitionerOK);
    }

    @Test
    @DisplayName("Test that patient is deleted after deleting")
    void testPatientIsDeletedFromDB() throws SQLException {
        patientDAO.createPatient(patient);

        patientDAO.deletePatient(patient);

        assertFalse(patientDAO.checkExists(patient));
    }
}
