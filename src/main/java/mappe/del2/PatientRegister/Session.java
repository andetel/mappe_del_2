package mappe.del2.PatientRegister;

import javafx.scene.Scene;
import mappe.del2.PatientRegister.Daos.PatientDAO;
import mappe.del2.PatientRegister.Models.Patient;
import mappe.del2.PatientRegister.Models.PatientRegister;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.sql.SQLException;
import java.util.List;

/**
 * Class to keep track of session details
 */
public class Session {
    private static final EntityManagerFactory emFactory = Persistence.createEntityManagerFactory("pu-register-derby");
    private static final EntityManager em = emFactory.createEntityManager();
    private static PatientRegister patientRegister;
    private static Scene scene;

    /**
     * Returns the current scene of the app
     *
     * @return Scene Object
     */
    public static Scene getScene() {
        return scene;
    }

    /**
     * Sets the current scene of the app
     *
     * @param scene
     *            new Scene Object
     */
    public static void setScene(Scene scene) {
        Session.scene = scene;
    }

    /**
     * Gets the PatientRegister
     *
     * @return PatientRegister Object
     */
    public static PatientRegister getPatientRegister() {
        return patientRegister;
    }

    /**
     * Sets a new PatientRegister
     *
     * @param patientRegister
     *            PatientRegister Object
     */
    public static void setPatientRegister(PatientRegister patientRegister) {
        Session.patientRegister = patientRegister;
    }

    /**
     * Saves new patient to database
     *
     * @param patient
     *            Patient Object to save
     *
     * @throws SQLException
     */
    public static void savePatient(Patient patient) throws SQLException {
        PatientDAO patientDAO = new PatientDAO(em);
        patientDAO.createPatient(patient);
    }

    /**
     * Gets every patient from the database
     *
     * @throws SQLException
     */
    public static List<Patient> getAllPatients() throws SQLException {
        PatientDAO patientDAO = new PatientDAO(em);
        return patientDAO.getEveryPatient();
    }

    /**
     * Deletes the given patient from the database
     *
     * @param patient
     *            Patient to delete
     */
    public static void deletePatient(Patient patient) throws SQLException {
        PatientDAO patientDAO = new PatientDAO(em);
        patientDAO.deletePatient(patient);
    }

    /**
     * Updates the given patient in the database
     *
     * @param patient
     *            patient to update
     *
     * @throws SQLException
     *             if database query fails
     */
    public static void editPatient(Patient patient) throws SQLException {
        PatientDAO patientDAO = new PatientDAO(em);
        patientDAO.updatePatient(patient);
    }

    /**
     * Checks if the given user already exists in the database
     *
     * @param patient
     *            patient to check
     *
     * @return true if, and only if, patient already exists
     *
     * @throws SQLException
     *             if database query fails
     *
     */
    public static boolean exists(Patient patient) throws SQLException {
        PatientDAO patientDAO = new PatientDAO(em);
        return patientDAO.checkExists(patient);
    }
}
