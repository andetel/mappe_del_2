package mappe.del2.PatientRegister.Daos;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

/**
 * Interface for DAO
 *
 * @param <T>
 *            Entity type the DAO interacts with
 */
public interface DAO<T> {

    /**
     * Gets an Object with type T
     *
     * @param id
     *            ID of object.
     *
     * @return T
     *
     * @throws SQLException
     *             if query fails
     *
     */
    Optional<T> getPatient(String id) throws SQLException;

    /**
     * Gets all Objects from database
     *
     * @return T
     *
     * @throws SQLException
     *             if query fails
     */
    List<T> getEveryPatient() throws SQLException;

    /**
     * Create instance of object T in database
     *
     * @param entity
     *            object to create
     *
     * @throws SQLException
     *             if query fails
     */
    void createPatient(T entity) throws SQLException;

    /**
     * Deletes given object from database
     *
     * @param entity
     *            object to delete
     *
     * @throws SQLException
     *             if query fails
     */
    void deletePatient(T entity) throws SQLException;

    /**
     * Checks if object exists in database
     *
     * @param entity
     *            object to check
     *
     * @return true if, and only if, object exists
     *
     * @throws SQLException
     *             if query fails
     */
    boolean checkExists(T entity) throws SQLException;

    /**
     * Updates the given patient in the database
     *
     */
    void updatePatient(T entity) throws SQLException;

}
