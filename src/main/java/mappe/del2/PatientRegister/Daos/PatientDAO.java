package mappe.del2.PatientRegister.Daos;

import mappe.del2.PatientRegister.Models.Patient;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * DAO for Patient Objects
 */
public class PatientDAO implements DAO<Patient> {
    private EntityManager entityManager;

    /**
     * Constructor that creates a new PatientDAO and an EntityManager
     *
     * @param entityManager
     *            Persistence entity manager
     */

    public PatientDAO(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /**
     * Gets a Patient with the given social security number
     *
     * @param ssn
     *            social security number of a patient
     *
     * @return Optional
     *
     */

    @Override
    public Optional<Patient> getPatient(String ssn) {
        try {
            Query query = entityManager.createQuery("SELECT c FROM Patient c WHERE c.socialSecurityNumber LIKE :ssn")
                    .setParameter("ssn", ssn);
            return Optional.of(Objects.requireNonNull((Patient) query.getSingleResult()));
        } catch (NullPointerException e) {
            return Optional.empty();
        } catch (Exception e) {
            throw new IllegalArgumentException(
                    "Patient with Social Security Number: " + ssn + " does not exist in database");
        }
    }

    /**
     * Gets a list of all patients fro database
     *
     * @return list
     *
     * @throws SQLException
     *             if, and only if, query fails
     */

    @Override
    public List<Patient> getEveryPatient() throws SQLException {
        Query query = entityManager.createQuery("SELECT c FROM Patient c");
        return query.getResultList();
    }

    /**
     * Creates a patient object if it does not already exist
     *
     * @param patient
     *            patient to add to database
     *
     * @throws SQLException
     *             if query fails
     *
     * @throws EntityExistsException
     *             if entity exists
     */
    @Override
    public void createPatient(Patient patient) throws SQLException, EntityExistsException {
        EntityTransaction transaction = null;
        try {
            transaction = entityManager.getTransaction();
            transaction.begin();
            if (checkExists(patient)) {
                throw new EntityExistsException("Patient already exists.");
            } else {
                entityManager.persist(patient);
            }
            transaction.commit();
        } catch (Exception e) {
            if (Objects.nonNull(transaction)) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw new SQLException(patient.getFirstName() + ", " + patient.getSocialSecurityNumber(), e.getMessage());
        }
    }

    /**
     * Deleted the given patient from database
     *
     * @param patient
     *            patient to delete from database
     *
     * @throws SQLException
     *             if query fails
     */
    @Override
    public void deletePatient(Patient patient) throws SQLException {
        EntityTransaction transaction = null;

        try {
            transaction = entityManager.getTransaction();
            transaction.begin();
            entityManager.remove(patient);
            transaction.commit();
        } catch (Exception e) {
            if (Objects.nonNull(transaction)) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw new SQLException(patient.getFirstName() + ", " + patient.getSocialSecurityNumber(), e.getMessage());
        }
    }

    /**
     * Checks if patient exists in database
     *
     * @param patient
     *            patient to check
     *
     * @return returns true if patient exists
     *
     * @throws SQLException
     *             if query fails
     */
    @Override
    public boolean checkExists(Patient patient) throws SQLException {
        return getEveryPatient().contains(patient);
    }

    /**
     * Updates the given patient in the database
     *
     * @param patient
     *            patient to update
     *
     * @throws SQLException
     */
    @Override
    public void updatePatient(Patient patient) throws SQLException {
        EntityTransaction transaction = null;

        try {
            transaction = entityManager.getTransaction();
            transaction.begin();
            entityManager.merge(patient);
            transaction.commit();
        } catch (Exception e) {
            if (Objects.nonNull(transaction)) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw new SQLException(String.format("Unable to update patient '%s': %s", patient.getSocialSecurityNumber(),
                    e.getMessage()));
        }
    }

}
