package mappe.del2.PatientRegister.Models;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Creates a PatientRegister
 */
public class PatientRegister {
    private ArrayList<Patient> patients;

    /**
     * Constructor for new PatientRegister
     */
    public PatientRegister() {
        this.patients = new ArrayList<Patient>();
    }

    /**
     * Gets all patients from the patients list
     *
     * @return ArrayList with Patient Objects
     */
    public List<Patient> getPatients() {
        return this.patients;
    }

    /**
     * Gets patient with the given social security number
     *
     * @param ssn
     *            social security number of patient
     *
     * @return {@Link Patient} object, or null if none
     */
    public Patient getPatientBySSN(String ssn) {
        return this.patients.stream().filter(p -> p.getSocialSecurityNumber().equals(ssn)).findAny().orElse(null);
    }

    /**
     * Adds a patient with the given social security number, and first and last name to the PatientRegister
     *
     * @param patient
     *            patient to add to the list
     *
     * @throws IllegalArgumentException
     *             if Patient is already in the list
     *
     */
    public void addPatient(Patient patient) throws IllegalArgumentException {
        if (Objects.nonNull(this.getPatientBySSN(patient.getSocialSecurityNumber()))) {
            throw new IllegalArgumentException("Patient with provided social security number is already in the list");
        }

        this.patients.add(patient);
    }

    /**
     * Gets a patient from the PatientRegister and changes first and/or last name if new are specified
     *
     * @param firstName
     *            patients current first name
     *
     * @param lastName
     *            patients current last name
     *
     * @param ssn
     *            patients social security number
     */
    public void editPatient(String firstName, String lastName, String ssn) {
        if (firstName.equals("") || lastName.equals("")) {
            throw new IllegalArgumentException("Patient needs both first and last name");
        }

        this.getPatientBySSN(ssn).setFirstName(firstName);
        this.getPatientBySSN(ssn).setLastName(lastName);
    }

    /**
     * Removes a patient from the PatientRegister with the given social security number
     *
     * @param ssn
     *            patients social security number
     *
     * @return true if, and only if, a patient is successfully removed from the list
     */
    public boolean removePatient(String ssn) {
        return this.patients.removeIf(p -> p.getSocialSecurityNumber().equals(ssn));
    }
}
