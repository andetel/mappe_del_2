package mappe.del2.PatientRegister.Models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Objects;

/**
 * Creates a Patient Object
 */
@Entity
public class Patient implements Serializable {
    @Id
    private String socialSecurityNumber;
    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;
    private String diagnosis;
    private String generalPractitioner;

    /**
     *
     */
    public Patient() {

    }

    /**
     * Constructor for Patient Objects
     *
     * @param socialSecurityNumber
     *            social security number
     *
     * @param firstName
     *            first name
     *
     * @param lastName
     *            last name
     *
     */
    public Patient(String socialSecurityNumber, String firstName, String lastName) {
        if (socialSecurityNumber.length() != 11) {
            throw new IllegalArgumentException("Social security number must be of length 11");
        }

        if (firstName.isEmpty() || lastName.isEmpty()) {
            throw new IllegalArgumentException("The patient needs both first name and last name");
        }

        this.socialSecurityNumber = socialSecurityNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.diagnosis = "";
        this.generalPractitioner = "";
    }

    /**
     * Constructor for Patient Objects
     *
     * @param socialSecurityNumber
     *            social security number
     *
     * @param firstName
     *            first name
     *
     * @param lastName
     *            last name
     *
     * @param diagnosis
     *            name of diagnosis
     *
     * @param generalPractitioner
     *            name of general practitioner
     *
     */
    public Patient(String socialSecurityNumber, String firstName, String lastName, String diagnosis,
            String generalPractitioner) {
        if (socialSecurityNumber.length() != 11) {
            throw new IllegalArgumentException("Social security number must be of length 11");
        }

        if (firstName.isEmpty() || lastName.isEmpty()) {
            throw new IllegalArgumentException("The patient needs both first name and last name");
        }

        this.socialSecurityNumber = socialSecurityNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.diagnosis = diagnosis;
        this.generalPractitioner = generalPractitioner;
    }

    /**
     * Gets the patients social security number
     *
     * @return String with patients social security number
     */
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    /**
     * Gets the first name of the patient
     *
     * @return String with patients first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the patients first name
     *
     * @param firstName
     *            new first name of patient
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets the patients last name
     *
     * @return String with last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the patients last name
     *
     * @param lastName
     *            new last name of patient
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets the patients diagnosis
     *
     * @return String with name of diagnosis
     */
    public String getDiagnosis() {
        return diagnosis;
    }

    /**
     * Sets a diagnosis on the patient
     *
     * @param diagnosis
     *            name of diagnosis
     */
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    /**
     * Gets the patients general practitioner
     *
     * @return String with name of general practitioner
     */
    public String getGeneralPractitioner() {
        return generalPractitioner;
    }

    /**
     * Sets name of patients general practitioner
     *
     * @param generalPractitioner
     *            name of general practitioner
     */
    public void setGeneralPractitioner(String generalPractitioner) {
        this.generalPractitioner = generalPractitioner;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (!(o instanceof Patient)) {
            return false;
        }

        Patient p = (Patient) o;

        return this.getSocialSecurityNumber().equals(p.getSocialSecurityNumber());
    }

    @Override
    public String toString() {
        return this.getFirstName() + " " + this.getLastName() + ", SSN: " + this.getSocialSecurityNumber();
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.socialSecurityNumber);
    }
}
