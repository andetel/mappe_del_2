package mappe.del2.PatientRegister.Controllers;

import com.jfoenix.controls.JFXButton;;
import com.jfoenix.controls.JFXTextField;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import mappe.del2.PatientRegister.App;
import mappe.del2.PatientRegister.Models.Patient;
import mappe.del2.PatientRegister.Session;
import mappe.del2.PatientRegister.Utils.CSVFile;
import mappe.del2.PatientRegister.Utils.DialogboxFactory;
import mappe.del2.PatientRegister.Utils.FXMLFile;
import org.apache.commons.io.FilenameUtils;

import java.io.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Controller for the main window, and for edit and delete patient windows
 */
public class MainWindowController {

    private Patient selectedPatient;

    @FXML
    private JFXTextField editFirstName;
    @FXML
    private JFXTextField editLastName;
    @FXML
    private JFXTextField editSocialSecurityNumber;

    @FXML
    private JFXButton addPatientButton;
    @FXML
    private JFXButton deletePatientButton;
    @FXML
    private JFXButton editPatientButton;

    @FXML
    private ImageView aboutInfoImage;
    @FXML
    private Label applicationNameLabel;
    @FXML
    private Label versionLabel;

    @FXML
    private Label statusLabel;

    @FXML
    private TableView<Patient> patientsList;
    @FXML
    private TableColumn<Patient, String> columnFirstName;
    @FXML
    private TableColumn<Patient, String> columnLastName;
    @FXML
    private TableColumn<Patient, String> columnSocialSecurityNumber;

    private ObservableList<Patient> observablePatientsList;

    /**
     * Method that runs upon main-window initialization
     */
    @FXML
    public void initialize() {

        // Create columns
        this.columnFirstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        this.columnLastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        this.columnSocialSecurityNumber.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));

        this.observablePatientsList = FXCollections.observableArrayList(Session.getPatientRegister().getPatients());

        this.patientsList.setItems(this.observablePatientsList);

        this.statusLabel.setText("OK");

        // Adds add, edit and delete icons to toolbar buttons
        Image add = new Image(String.valueOf(App.class.getResource("img/add-user.png")));
        ImageView addView = new ImageView(add);
        addView.setFitHeight(25);
        addView.setPreserveRatio(true);

        Image edit = new Image(String.valueOf(App.class.getResource("img/edit-user.png")));
        ImageView editView = new ImageView(edit);
        editView.setFitHeight(25);
        editView.setPreserveRatio(true);

        Image delete = new Image(String.valueOf(App.class.getResource("img/delete-user.png")));
        ImageView deleteView = new ImageView(delete);
        deleteView.setFitHeight(25);
        deleteView.setPreserveRatio(true);

        this.addPatientButton.setGraphic(addView);
        this.addPatientButton.getStyleClass().add("add-edit-delete-button");
        this.editPatientButton.setGraphic(editView);
        this.editPatientButton.getStyleClass().add("add-edit-delete-button");
        this.deletePatientButton.setGraphic(deleteView);
        this.deletePatientButton.getStyleClass().add("add-edit-delete-button");

    }

    /**
     * Shows the about view when about is clicked in the menu
     *
     * @throws IOException
     *             if unable to load fxml file
     */
    @FXML
    public void showAbout() throws IOException {
        Stage about = new Stage();

        try {
            about.setScene(new Scene(FXMLFile.load("about", this)));
            about.setTitle("Information Dialog - About");
            about.getScene().getStylesheets().clear();
            about.getScene().getStylesheets().add(App.class.getResource("css/style.css").toString());
            this.applicationNameLabel.setText("Patient Register");

            Image image = new Image(String.valueOf(App.class.getResource("img/info.png")));
            this.aboutInfoImage.setImage(image);
            this.aboutInfoImage.setFitHeight(50);
            this.aboutInfoImage.setPreserveRatio(true);

            Session.setScene(about.getScene());

            about.show();

        } catch (IOException e) {
            e.printStackTrace();
            DialogboxFactory.getDialog("Load failed", "Unable to load new window").show();
        }
    }

    /**
     * Loads the add-patient view when add patient is clicked
     *
     * @throws IOException
     *             if unable to load fxml file
     */
    @FXML
    public void showAddPatient() throws IOException {
        Stage addPatient = new Stage();

        int listLength = Session.getPatientRegister().getPatients().size();

        try {
            addPatient.setScene(new Scene(FXMLFile.load("add-patient")));
            addPatient.setTitle("Patient details - add");
            addPatient.getScene().getStylesheets().clear();
            addPatient.getScene().getStylesheets().add(App.class.getResource("css/style.css").toString());

            Session.setScene(addPatient.getScene());

            addPatient.showAndWait();

            if (listLength == Session.getPatientRegister().getPatients().size()) {
                this.statusLabel.setText("Add failed");
            } else if (listLength < Session.getPatientRegister().getPatients().size()) {
                this.statusLabel.setText("Add successful");
            }

            updateObservableList();

        } catch (IOException e) {
            e.printStackTrace();
            DialogboxFactory.getDialog("Load failed", "Unable to load new window").show();
        }

    }

    /**
     * Loads the delete-patient view when delete patient is clicked.
     *
     * @throws IOException
     *             if unable to load fxml file
     */
    @FXML
    public void showDeletePatient() throws IOException {
        Stage deletePatient = new Stage();

        try {
            if (Session.getPatientRegister().getPatients().size() == 0) {
                throw new IllegalAccessException(
                        "There needs to be at least one patient in the list before trying to delete.");
            }

            deletePatient.setScene(new Scene(FXMLFile.load("delete-patient", this)));
            deletePatient.setTitle("Delete confirmation");
            deletePatient.getScene().getStylesheets().add(App.class.getResource("css/style.css").toString());

            Session.setScene(deletePatient.getScene());

            deletePatient.showAndWait();
            updateObservableList();

        } catch (IOException e) {
            e.printStackTrace();
            DialogboxFactory.getDialog("Load failed", "Unable to load new window").show();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            DialogboxFactory.getDialog("Error", e.getMessage()).show();
        }
    }

    /**
     * Loads the edit-patient view when edit patient is clicked.
     *
     * @throws IOException
     *             if unable to load fxml file
     */
    @FXML
    public void showEditPatient() throws IOException {
        Stage editPatient = new Stage();

        try {
            if (Session.getPatientRegister().getPatients().size() == 0) {
                throw new IllegalAccessException("There needs to be at least one patient in the list first.");
            }

            editPatient.setScene(new Scene(FXMLFile.load("edit-patient", this)));
            editPatient.setTitle("Patient details - edit");
            editPatient.getScene().getStylesheets().add(App.class.getResource("css/style.css").toString());

            Session.setScene(editPatient.getScene());

            this.editFirstName.setText(this.selectedPatient.getFirstName());
            this.editLastName.setText(this.selectedPatient.getLastName());
            this.editSocialSecurityNumber.setText(this.selectedPatient.getSocialSecurityNumber());

            editPatient.showAndWait();
            updateObservableList();

        } catch (IOException e) {
            e.printStackTrace();
            DialogboxFactory.getDialog("Load failed", "Unable to load new window").show();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            DialogboxFactory
                    .getDialog("Error", "There needs to be at least one patient in the list before trying to edit.")
                    .show();
        }

    }

    /**
     * Updates the observablePatientsList with all new, edited or deleted patients.
     */
    public void updateObservableList() {
        this.observablePatientsList.setAll(Session.getPatientRegister().getPatients());
    }

    /**
     * Gets the current scene from the session and closes the stage connected to it.
     */
    public void closeWindow() {
        Stage currentWindow = (Stage) Session.getScene().getWindow();

        currentWindow.close();
    }

    /**
     * Deletes the chosen patient from the register.
     */
    public void deletePatient() {

        try {
            Session.deletePatient(selectedPatient);
        } catch (SQLException e) {
            e.printStackTrace();
            DialogboxFactory
                    .getDialog("Delete failed",
                            String.format("Not able to delete patient from database.\nError: %s", e.getMessage()))
                    .show();
        }

        if (Session.getPatientRegister().removePatient(selectedPatient.getSocialSecurityNumber())) {
            this.statusLabel.setText("Patient successfully removed");
        } else {
            this.statusLabel.setText("Patient was not removed");
        }

        closeWindow();
    }

    /**
     * Method that runs when a user clicks on edit patient. Gets text from first and last name fields and updates the
     * selected patient with the new details.
     */
    public void editPatient() {

        try {

            String newFirstName = this.editFirstName.getText();
            String newLastName = this.editLastName.getText();

            if (newFirstName.equals("") || newLastName.equals("")) {
                throw new IllegalArgumentException("A patient needs both first and last name.");
            }

            this.selectedPatient.setFirstName(newFirstName);
            this.selectedPatient.setLastName(newLastName);

            Session.editPatient(this.selectedPatient);

            Session.getPatientRegister().editPatient(newFirstName, newLastName,
                    this.selectedPatient.getSocialSecurityNumber());

            this.statusLabel.setText("Edit successful");
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            DialogboxFactory
                    .getDialog("Edit failed", String.format("Not able to commit changes.\nError: %s", e.getMessage()))
                    .show();
            this.statusLabel.setText("Changes could not be committed");
        } catch (SQLException e) {
            e.printStackTrace();
            DialogboxFactory.getDialog("Edit failed",
                    String.format("Not able to add changes to database.\nError: %s", e.getMessage())).show();
        }

        closeWindow();
    }

    /**
     * Gets the selected patient and stores it in the variable selectedPatient
     */
    public void controlSelectedPatient() {
        this.selectedPatient = patientsList.getSelectionModel().getSelectedItem();
    }

    /**
     * Sends a System.exit(0) to the application and terminates it
     */
    public void terminateApplication() {
        System.exit(0);
    }

    /**
     * Opens a new FileChooser and imports the chosen .csv file
     */
    public void importCSV() {
        List<Patient> tmpPatientsList = new ArrayList<Patient>();

        try {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Choose file");
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv"));
            File file = fileChooser.showOpenDialog(this.patientsList.getScene().getWindow());

            if (!FilenameUtils.getExtension(file.getName()).equals("csv")) {
                throw new IOException("Invalid file type. File must be of type .csv.");
            }

            for (String[] arr : CSVFile.read(file)) {
                String fn = arr[0];
                String ln = arr[1];
                String gp = arr[2];
                String ssn = arr[3];

                if (ssn.length() == 11) {
                    Patient patient = new Patient(ssn, fn, ln, "", gp);
                    tmpPatientsList.add(patient);
                }

            }

            Session.getPatientRegister().getPatients().clear();
            Session.getPatientRegister().getPatients().addAll(tmpPatientsList);

            this.statusLabel.setText("Import successful");

        } catch (IOException e) {
            e.printStackTrace();
            DialogboxFactory.getDialog("Invalid file", e.getMessage()).show();
            this.statusLabel.setText("Import failed");
        }

        updateObservableList();

    }

    /**
     * Opens a new FileChooser in save mode and writes every patients in Session.getPatientRegister().getPatients() to a
     * new/or replaces an existing .csv file
     */
    public void exportCSV() {

        List<String[]> tmpPatientsList = new ArrayList<String[]>();

        for (Patient patient : Session.getPatientRegister().getPatients()) {
            String[] pd = { patient.getFirstName(), patient.getLastName(), patient.getGeneralPractitioner(),
                    patient.getSocialSecurityNumber() };

            tmpPatientsList.add(pd);
        }

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save file");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv"));

        File file = fileChooser.showSaveDialog(this.patientsList.getScene().getWindow());

        if (Objects.nonNull(file)) {
            CSVFile.write(tmpPatientsList, file);
            this.statusLabel.setText("write success");
        }

    }

    /**
     * Imports every patient from the database and adds them to the table
     *
     * @throws SQLException
     *             if database query fails
     *
     */
    public void importFromDB() throws SQLException {
        List<Patient> tmpPatients = Session.getAllPatients();
        Session.getPatientRegister().getPatients().addAll(tmpPatients);
        updateObservableList();
    }

    /**
     * Saves all patients to database
     *
     * @throws SQLException
     *             if database query fails
     *
     */
    public void saveToDB() throws SQLException {
        for (Patient patient : Session.getPatientRegister().getPatients()) {
            if (Session.exists(patient)) {
                Session.editPatient(patient);
            } else {
                Session.savePatient(patient);
            }
        }

        this.statusLabel.setText("Database save successful");
    }
}
