package mappe.del2.PatientRegister.Controllers;

import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.stage.Stage;
import mappe.del2.PatientRegister.Models.Patient;
import mappe.del2.PatientRegister.Session;
import mappe.del2.PatientRegister.Utils.DialogboxFactory;

import java.sql.SQLException;

/**
 * Controller for the add patient window
 */
public class AddPatientController {
    @FXML
    private JFXTextField firstNameField;
    @FXML
    private JFXTextField lastNameField;
    @FXML
    private JFXTextField socialSecurityNumberField;

    /**
     * Gets the new patients last and first name and social security number from the firstNameField, lastNameField and
     * socialSecurityField and adds the patient to the PatientRegister.
     */
    public void addNewPatient() throws IllegalArgumentException {
        String firstName = this.firstNameField.getText();
        String lastName = this.lastNameField.getText();
        String socialSecurityNumber = this.socialSecurityNumberField.getText();

        try {
            Patient patient = new Patient(socialSecurityNumber, firstName, lastName);
            Session.getPatientRegister().addPatient(patient);
            Session.savePatient(patient);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            DialogboxFactory.getDialog("Error", String.format("Error adding patient.%nError: %s", e.getMessage()))
                    .show();
        } catch (SQLException e) {
            e.printStackTrace();
            DialogboxFactory
                    .getDialog("Error", String.format("Error adding patient to database.%nError: %s", e.getMessage()))
                    .show();
        }

        closeWindow();
    }

    /**
     * Gets the active window of the scene and closes it.
     */
    public void closeWindow() {
        Stage currentWindow = (Stage) this.firstNameField.getScene().getWindow();

        currentWindow.close();
    }
}
