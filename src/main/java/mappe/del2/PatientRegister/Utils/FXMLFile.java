package mappe.del2.PatientRegister.Utils;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import mappe.del2.PatientRegister.App;

import java.io.IOException;

/**
 * Class for loading FXML files
 */
public class FXMLFile {

    /**
     * Loads a new FXML file with the given file name
     *
     * @param name
     *            name of FXML file
     *
     * @return Parent node
     *
     * @throws IOException
     *             if not able to load file
     *
     */
    public static Parent load(String name) throws IOException {
        String path = String.format("fxml/%s.fxml", name);
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(path));
        return fxmlLoader.load();
    }

    /**
     * Loads a new FXML file with the given file name and controller
     *
     * @param name
     *            name of FXML file
     *
     * @param parentCorntroller
     *            parentcontroller object
     *
     * @return Parent node
     *
     * @throws IOException
     *             if not able to load file
     *
     */
    public static Parent load(String name, Object parentCorntroller) throws IOException {
        String path = String.format("fxml/%s.fxml", name);
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(path));
        fxmlLoader.setController(parentCorntroller);
        return fxmlLoader.load();
    }
}