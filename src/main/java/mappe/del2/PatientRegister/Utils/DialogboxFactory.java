package mappe.del2.PatientRegister.Utils;

import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;

/**
 * Factory for creating dialog boxes
 */
public class DialogboxFactory {

    /**
     * Creates a new dialog box with an OK button
     *
     * @param title
     *            title of the dialog box
     *
     * @param content
     *            content to be displayed inside the box
     *
     * @return new Dialog Object
     */
    public static Dialog<ButtonType> getDialog(String title, String content) {
        Dialog<ButtonType> dialog = new Dialog<ButtonType>();
        dialog.setTitle(title);

        dialog.setContentText(content);

        ButtonType btnType = new ButtonType("OK", ButtonBar.ButtonData.OK_DONE);

        dialog.getDialogPane().getButtonTypes().add(btnType);

        return dialog;
    }

}
