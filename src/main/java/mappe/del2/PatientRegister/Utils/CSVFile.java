package mappe.del2.PatientRegister.Utils;

import com.opencsv.*;
import com.opencsv.exceptions.CsvException;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * Class for easier handling of reading and writing data to and from .csv files
 */
public class CSVFile {

    /**
     * Reads the supplied file and returns a two dimensional list of string arrays
     *
     * @param file
     *            chosen input file from FileChooser
     *
     * @return two dimensional list of strings
     */
    public static List<String[]> read(File file) {
        List<String[]> data = null;

        try (FileReader fileReader = new FileReader(file)) {

            CSVParser csvParser = new CSVParserBuilder().withSeparator(';').build();

            CSVReader csvReader = new CSVReaderBuilder(fileReader).withCSVParser(csvParser).withSkipLines(1).build();

            data = csvReader.readAll();

        } catch (IOException | CsvException e) {
            e.printStackTrace();
        }

        return data;
    }

    /**
     * Writes the content of lst to the chosen file
     *
     * @param lst
     *            content to be written
     *
     * @param file
     *            chosen file from FileChooser
     */
    public static void write(List<String[]> lst, File file) {
        try (FileWriter outputfile = new FileWriter(file)) {

            CSVWriter writer = new CSVWriter(outputfile, ';', CSVWriter.NO_QUOTE_CHARACTER,
                    CSVWriter.DEFAULT_ESCAPE_CHARACTER, CSVWriter.DEFAULT_LINE_END);

            writer.writeNext(new String[] { "firstName", "lastName", "generalPractitioner", "socialSecurityNumber" });
            writer.writeAll(lst);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
