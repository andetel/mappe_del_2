package mappe.del2.PatientRegister;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import mappe.del2.PatientRegister.Models.PatientRegister;
import mappe.del2.PatientRegister.Utils.FXMLFile;

import java.io.IOException;

/**
 * Application entry
 */
public class App extends Application {
    private static final String WINDOW_TITLE = "Patient register";

    /**
     * Starts the JavaFX Application
     *
     * @param stage
     *            a Stage Object sent bu JavaFX
     *
     * @throws Exception
     *             if there is any problems starting the application
     */
    @Override
    public void start(Stage stage) throws Exception {
        Session.setPatientRegister(new PatientRegister());

        try {
            Scene scene = new Scene(FXMLFile.load("main-window"));
            stage.setScene(scene);
            stage.setResizable(false);

            scene.getStylesheets().clear();
            scene.getStylesheets().add(getClass().getResource("css/style.css").toString());

            Session.setScene(scene);

            stage.setTitle(WINDOW_TITLE);
            stage.show();
        } catch (IOException e) {
            e.getCause();
        }

    }

    public static void main(String[] args) {
        launch();
    }
}
