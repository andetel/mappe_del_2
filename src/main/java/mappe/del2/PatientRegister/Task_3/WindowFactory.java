package mappe.del2.PatientRegister.Task_3;

import mappe.del2.PatientRegister.Task_3.Factory.MenuNodes;
import mappe.del2.PatientRegister.Task_3.Factory.Nodes;
import mappe.del2.PatientRegister.Task_3.Factory.SetWindow;
import mappe.del2.PatientRegister.Task_3.Factory.Table;

/**
 * Factory to get the desired factory
 */
public class WindowFactory {
    public Nodes getDesiredWindow(Table mainTable, MenuNodes menuNodes) {
        return new SetWindow(mainTable, menuNodes);
    }
}

/**
 * Example on how to use the "factory"
 */
class WindowFactoryExample {
    WindowFactory windowFactory = new WindowFactory();

    MenuNodes menuNodes = new MenuNodes("Menu");
    Table table = new Table("Register Table");

    Nodes mainWindow = windowFactory.getDesiredWindow(table, menuNodes);
}
