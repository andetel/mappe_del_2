package mappe.del2.PatientRegister.Task_3.Factory;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;

/**
 * Class to set a menu with specific name and items
 */
public class MenuNodes {
    private String nameOfMenu;

    private Menu fileMenuOption = new Menu("File");
    private Menu editMenuOption = new Menu("Edit");
    private Menu helpMenuOption = new Menu("Help");
    private MenuBar menuBarOption = new MenuBar();

    /**
     * Constructor to create a MenuNode Object
     *
     * @param typeOfMenu
     */
    public MenuNodes(String typeOfMenu) {
        this.menuBarOption.getMenus().addAll(getFileMenu(), getEditMenu(), getHelpMenu());
        this.nameOfMenu = typeOfMenu;
    }

    /**
     * Gets a menu option for a files-menu
     *
     * @return Menu Object with the desired MenuItems inside
     */
    public Menu getFileMenu() {
        fileMenuOption.getItems().add(new MenuItem("Load Database"));
        fileMenuOption.getItems().add(new MenuItem("Save Database"));
        fileMenuOption.getItems().add(new SeparatorMenuItem());
        fileMenuOption.getItems().add(new MenuItem("Import from .csv"));
        fileMenuOption.getItems().add(new MenuItem("Export to csv"));
        fileMenuOption.getItems().add(new SeparatorMenuItem());
        fileMenuOption.getItems().add(new MenuItem("Exit"));
        return fileMenuOption;
    }

    /**
     * Gets a menu option for a edit-menu
     *
     * @return Menu Object with desired MenuItems
     */
    public Menu getEditMenu() {
        editMenuOption.getItems().add(new MenuItem("Add patient"));
        editMenuOption.getItems().add(new MenuItem("Remove patient"));
        editMenuOption.getItems().add(new MenuItem("Edit patient"));
        return editMenuOption;
    }

    /**
     * Gets a menu option for a help-menu
     *
     * @return Menu Object with desired MenuItems
     */
    public Menu getHelpMenu() {
        helpMenuOption.getItems().add(new MenuItem("About"));
        return helpMenuOption;
    }

    /**
     * Gets the name of the table
     *
     * @return String with the name of the menu
     */
    public String getNameOfTable() {
        return nameOfMenu;
    }
}
