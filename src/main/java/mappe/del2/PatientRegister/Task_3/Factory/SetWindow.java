package mappe.del2.PatientRegister.Task_3.Factory;

/**
 * Class to set the window
 */
public class SetWindow implements Nodes {
    private Table table;
    private MenuNodes menuNodes;

    /**
     * Constructor for a new Window
     *
     * @param table
     *            Table Object to insert into the window
     * @param menuNodes
     *            MenuNode Object to insert into the window
     */
    public SetWindow(Table table, MenuNodes menuNodes) {
        this.table = table;
        this.menuNodes = menuNodes;
    }

    /**
     * Sets a new menu to the window
     *
     * @param menuNodes
     *            the desired menu to insert into the window
     */
    @Override
    public void setMenu(MenuNodes menuNodes) {
        this.menuNodes = menuNodes;
    }

    /**
     * Sets a new table to the window
     *
     * @param table
     *            the desired table to insert into the window
     */
    @Override
    public void setTable(Table table) {
        this.table = table;
    }
}
