package mappe.del2.PatientRegister.Task_3.Factory;

/**
 * Interface for the nodes of the window
 */
public interface Nodes {
    void setMenu(MenuNodes menuNodes);

    void setTable(Table table);
}
