package mappe.del2.PatientRegister.Task_3.Factory;

import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import mappe.del2.PatientRegister.Models.Patient;

/**
 * Class to set a type of table with specific columns.
 */
public class Table {
    private String nameOfTable;
    private TableView<Patient> tableView = new TableView<>();

    private TableColumn<Patient, String> firstNameColumn = new TableColumn<>("First Name");
    private TableColumn<Patient, String> lastNameColumn = new TableColumn<>("Last Name");
    private TableColumn<Patient, String> socialSecurityNumberColumn = new TableColumn<>("Social Security Number");

    /**
     * Constructor for a new Table Object
     *
     * @param table
     */
    public Table(String table) {
        this.tableView.getColumns().addAll(getFirstNameColumn(), getLastNameColumn(), getSocialSecurityColumnColumn());
        this.nameOfTable = table;
    }

    /**
     * Gets table column for first name
     *
     * @return TableColumn Object
     */
    public TableColumn<Patient, String> getFirstNameColumn() {
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        return firstNameColumn;
    }

    /**
     * Gets table column of last name
     *
     * @return TableColumn Object
     */
    public TableColumn<Patient, String> getLastNameColumn() {
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        return lastNameColumn;
    }

    /**
     * Gets table column of social security number
     *
     * @return TableColumn Object
     */
    public TableColumn<Patient, String> getSocialSecurityColumnColumn() {
        socialSecurityNumberColumn.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));
        return socialSecurityNumberColumn;
    }

    /**
     * Gets the name of the table
     *
     * @return String with name of table
     */
    public String getNameOfTable() {
        return nameOfTable;
    }
}
